# README #

This repo contains backend component of a demo medic application.

### What is this repository for? ###

* War springboot component
* Beta version
* https://bitbucket.org/NivekARMR/mediapp-backend

### How do I get set up? ###

* Modify database field of application.properties
* Dependencies
* Database Postgresql.
* Run Testcases with Junit.
* Copy the component in jboss application server.
