package com.example.service;

import java.util.List;

import com.example.model.ConsultaExamen;

public interface IConsultaExamenService {
	
	List<ConsultaExamen> listarExamenesPorConsulta(Integer idconsulta);

}
