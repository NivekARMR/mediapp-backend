package com.example.service;

import java.util.List;

import com.example.dto.ConsultaListaExamenDTO;
import com.example.dto.ConsultaResumenDTO;
import com.example.dto.FiltroConsultaDTO;
import com.example.model.Consulta;

public interface IConsultaService {

	Consulta registrar(ConsultaListaExamenDTO consultaDTO);

	void modificar(Consulta consulta);

	void eliminar(int idConsulta);

	Consulta listarId(int idConsulta);

	List<Consulta> listar();
	
	List<Consulta> buscar(FiltroConsultaDTO filtro);

	List<Consulta> buscarfecha(FiltroConsultaDTO filtro);
	
	List<ConsultaResumenDTO> listarResumen();

	byte[] generarReporte();
}
