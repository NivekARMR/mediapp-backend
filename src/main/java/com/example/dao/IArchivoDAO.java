package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Archivo;

public interface IArchivoDAO extends JpaRepository<Archivo, Integer>{

}
