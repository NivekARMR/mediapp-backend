package com.example.dto;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.example.exception.ExceptionResponse;
import com.example.exception.ModeloNotFoundException;
import com.example.model.Consulta;
import com.example.model.Examen;
import com.example.model.Medico;
import com.example.model.Paciente;

public class DTOTest {

	ConsultaDTO consultadto;
	ConsultaListaExamenDTO consultaListaExamen;
	ConsultaResumenDTO consultaresumendto;
	FiltroConsultaDTO filtroconsultadto;
	MedicoDTO medicodto;
	PacienteDTO pacientedto;

	ExceptionResponse exception;
	ModeloNotFoundException modelo; 
	@Before
	public void setUp() throws Exception {
	consultadto= new ConsultaDTO();
	consultadto.setIdConsulta(1);
	consultadto.setMedico(new Medico());
	consultadto.setPaciente(new Paciente());

	consultaListaExamen= new ConsultaListaExamenDTO();
	consultaListaExamen.setConsulta(new Consulta());
	List<Examen> lstexamen= new ArrayList<Examen>();
	lstexamen.add(new Examen());
	consultaListaExamen.setLstExamen(lstexamen);
	
	consultaresumendto= new ConsultaResumenDTO();
	consultaresumendto.setCantidad(1);
	consultaresumendto.setFecha("12/12/2019");

	filtroconsultadto=new FiltroConsultaDTO();
	filtroconsultadto.setDni("71819234");
	filtroconsultadto.setFechaConsulta(LocalDateTime.now());
	filtroconsultadto.setNombreCompleto("71819234"); 
	
	medicodto = new MedicoDTO();
	medicodto.setIdMedico(1);
	medicodto.setNombre("Kevin");
	
	pacientedto= new PacienteDTO();
	pacientedto.setIdPaciente(1);
	pacientedto.setNombresCompletos("Kevin Meneses");
	
	exception = new ExceptionResponse(new Date(),"Error excetion","detail Error");
	exception.setDetalles("Error!!!");
	exception.setMensaje("Detail Error!!!");
	exception.setDetalles("Error!!!");
	exception.setTimestamp(new Date());

	modelo= new ModeloNotFoundException("ERROR");
	}

	@Test
	public void test() {
		assertTrue(consultadto.getIdConsulta()!=0);
		assertTrue(consultadto.getMedico()!=null);
		assertTrue(consultadto.getPaciente()!=null);

		assertTrue(consultaListaExamen.getConsulta()!=null);
		assertTrue(consultaListaExamen.getLstExamen()!=null); 
		
		assertTrue(consultaresumendto.getCantidad()!=0);
		assertTrue(consultaresumendto.getFecha()!=null); 

		assertTrue(filtroconsultadto.getDni()!=null);
		assertTrue(filtroconsultadto.getNombreCompleto()!=null);
		assertTrue(filtroconsultadto.getFechaConsulta()!=null); 

		assertTrue(medicodto.getIdMedico()!=0);
		assertTrue(medicodto.getNombre()!=null); 

		assertTrue(pacientedto.getIdPaciente()!=0);
		assertTrue(pacientedto.getNombresCompletos()!=null); 

		
		ConsultaResumenDTO consultaresumendto2=  new ConsultaResumenDTO(2,"a");
		assertTrue(consultaresumendto2!=null);
	
		assertTrue(exception.getDetalles()!=null);
		assertTrue(exception.getMensaje()!=null);
		assertTrue(exception.getTimestamp()!=null);
	
	}

}
