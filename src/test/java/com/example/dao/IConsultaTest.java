package com.example.dao;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Consulta;
import com.example.model.DetalleConsulta;
import com.example.model.Especialidad;
import com.example.model.Medico;
import com.example.model.Paciente;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IConsultaTest {


	@Autowired
	private IConsultaDAO consultadao;

	@Autowired
	private IPacienteDAO pacientedao;

	@Autowired
	private IEspecialidadDAO especialidaddao;

	@Autowired
	private IMedicoDAO medicodao;
 
 
	@Test
	public void crearConsulta() {
		
		Paciente paciente= new Paciente();
 
		paciente.setIdPaciente(1);
		paciente.setNombres("kevin");
		paciente.setApellidos("meneses");
		paciente.setDireccion("Vrela 1176"); 
		paciente.setDni("71819234");
		paciente.setTelefono("957264681");

		pacientedao.save(paciente);

		Medico medico = new Medico();
		medico.setIdMedico(1);
		medico.setNombres("kevin");
		medico.setApellidos("meneses");
		medico.setCMP("1544656"); 
		medicodao.save(medico);
		
		Especialidad especialidad= new Especialidad();
		especialidad.setIdEspecialidad(1);
		especialidad.setNombre("Urologia");
		Especialidad nuevoe=especialidaddao.save(especialidad);
		
		Consulta con = new Consulta();
		
		con.setIdConsulta(1);
		con.setPaciente(paciente);
		con.setMedico(medico);
		con.setEspecialidad(especialidad);
		con.setFecha(LocalDateTime.now());
		List<DetalleConsulta> lista= new ArrayList<DetalleConsulta>();
		DetalleConsulta detalleConsulta= new DetalleConsulta();
		detalleConsulta.setIdDetalle(1);
		detalleConsulta.setConsulta(con);
		detalleConsulta.setDiagnostico("D");
		detalleConsulta.setDiagnostico("Diagnostico");
		detalleConsulta.setTratamiento("Tratamiento");
		lista.add(detalleConsulta);
		
		con.setDetalleConsulta(lista);
		Consulta nuevo=consultadao.save(con);

		assertTrue(nuevo.getEspecialidad()!=null);
		assertTrue(nuevo.getMedico()!=null);
		assertTrue(nuevo.getPaciente()!=null);
		assertTrue(nuevo.getEspecialidad()!=null);
		assertTrue(nuevo.getDetalleConsulta()!=null);
		assertTrue(nuevo.getFecha()!=null);
		assertTrue(nuevoe.getNombre().equals(especialidad.getNombre()));

	} 
 	
}
