package com.example.dao;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Archivo;
import com.example.model.Consulta;
import com.example.model.DetalleConsulta;
import com.example.model.Especialidad;
import com.example.model.Medico;
import com.example.model.Menu;
import com.example.model.Paciente;
import com.example.model.Rol;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IMenuTest {

	
	@Autowired
	private IRolDAO roldao;

	@Autowired
	private IMenuDAO menudao;
  
	@Test
	public void crearMenu() {
		Rol rol = new Rol();
		rol.setDescripcion("Admin");
		rol.setIdRol(1);
		rol.setNombre("Administrator");
		Rol nuevo=roldao.save(rol);
		
		List<Rol> roles= new ArrayList();
		roles.add(nuevo);
	
		Menu menu= new Menu();
		menu.setIcono("añadir.jpg");
		menu.setIdMenu(1);
		menu.setNombre("Añadir Usuario");
		menu.setRoles(roles);
		menu.setUrl("../adduser/");  
		
		Menu nuevom=menudao.save(menu);
		
		assertTrue(nuevom.getIcono().equals(menu.getIcono()));
		assertTrue(nuevom.getNombre().equals(menu.getNombre())); 
		assertTrue(nuevom.getIdMenu()==(menu.getIdMenu())); 
		assertTrue(nuevom.getUrl()==(menu.getUrl())); 
		assertTrue(nuevom.getRoles()!=null); 
	}
}
