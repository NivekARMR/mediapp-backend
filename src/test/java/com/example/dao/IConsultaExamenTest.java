package com.example.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Consulta;
import com.example.model.ConsultaExamen;
import com.example.model.ConsultaExamenPK;
import com.example.model.DetalleConsulta;
import com.example.model.Especialidad;
import com.example.model.Examen;
import com.example.model.Medico;
import com.example.model.Paciente;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IConsultaExamenTest {

	@Autowired
	private IConsultaDAO consultadao;

	@Autowired
	private IPacienteDAO pacientedao;

	@Autowired
	private IEspecialidadDAO especialidaddao;

	@Autowired
	private IMedicoDAO medicodao;

	@Autowired
	private IExamenDAO examendao;

	@Test
	public void crearConsulta() {

		Paciente paciente = new Paciente();

		paciente.setIdPaciente(1);
		paciente.setNombres("kevin");
		paciente.setApellidos("meneses");
		paciente.setDireccion("Vrela 1176");
		paciente.setDni("71819234");
		paciente.setTelefono("957264681");

		pacientedao.save(paciente);

		Medico medico = new Medico();
		medico.setIdMedico(1);
		medico.setNombres("kevin");
		medico.setApellidos("meneses");
		medico.setCMP("1544656");
		medicodao.save(medico);

		Especialidad especialidad = new Especialidad();
		especialidad.setIdEspecialidad(1);
		especialidad.setNombre("Urologia");
		especialidaddao.save(especialidad);

		Consulta con = new Consulta();

		con.setIdConsulta(1);
		con.setPaciente(paciente);
		con.setMedico(medico);
		con.setEspecialidad(especialidad);

		List<DetalleConsulta> lista = new ArrayList<DetalleConsulta>();

		DetalleConsulta detalleConsulta = new DetalleConsulta();
		detalleConsulta.setIdDetalle(1);
		detalleConsulta.setConsulta(con);
		detalleConsulta.setDiagnostico("D");
		detalleConsulta.setDiagnostico("Diagnostico");
		detalleConsulta.setTratamiento("Tratamiento");
		lista.add(detalleConsulta);

		con.setDetalleConsulta(lista);
		Consulta nuevoc = consultadao.save(con);

		Examen examen = new Examen();
		examen.setIdExamen(1);
		examen.setNombre("Examen de Orina");
		examen.setDescripcion("Examen de Orina");
		Examen nuevoex = examendao.save(examen);

		ConsultaExamen consultaExamen = new ConsultaExamen();
		consultaExamen.setConsulta(nuevoc);
		consultaExamen.setExamen(nuevoex);
		ConsultaExamenPK pk1 = new ConsultaExamenPK();
		ConsultaExamenPK pk2 = new ConsultaExamenPK();
		ConsultaExamenPK pk3 = new ConsultaExamenPK();
		ConsultaExamenPK pk4 = new ConsultaExamenPK();
		ConsultaExamenPK pk5 = new ConsultaExamenPK();
		ConsultaExamenPK pk6 = new ConsultaExamenPK();
		ConsultaExamenPK pk7 = new ConsultaExamenPK();
		ConsultaExamenPK pk8 = new ConsultaExamenPK();
		ConsultaExamenPK pk9 = new ConsultaExamenPK();
		ConsultaExamenPK pk10 = new ConsultaExamenPK();
 
		assertTrue(pk1.equals(pk2));

		assertTrue(pk1.equals(pk1));
		pk3.setConsulta(null);
		assertTrue(pk4.equals(pk2));
		pk2.setExamen(new Examen());
		assertTrue(!pk1.equals(pk2));
		pk4.setExamen(null); 
		assertTrue(pk4.equals(pk3));
		assertTrue(!pk1.equals(null));
		assertTrue(pk1.hashCode() > 0);
		assertTrue(consultaExamen.getConsulta() != null);
		assertTrue(consultaExamen.getExamen() != null);
		assertTrue(lista.get(0).getIdDetalle() != 0);
		assertTrue(lista.get(0).getDiagnostico() != null);
		assertTrue(lista.get(0).getTratamiento() != null);
		assertTrue(lista.get(0).getConsulta() != null);
		assertTrue(!pk4.equals(new Object()));
		Examen examenEq = new Examen();
		Consulta consultaEq = new Consulta();
		pk5.setConsulta(new Consulta());
		pk5.setExamen(examenEq);
		pk7.setConsulta(consultaEq);
		pk7.setExamen(examenEq);
		pk8.setExamen(new Examen());
		pk8.setConsulta(consultaEq);
		pk9.setExamen(null);
		pk9.setConsulta(consultaEq);
		Consulta a = new Consulta();
		a.setIdConsulta(100);
		pk10.setExamen(new Examen());
		pk10.setConsulta(a);
		assertTrue(!pk5.equals(pk6));
		assertTrue(!pk5.equals(pk4));
		assertTrue(!pk6.equals(pk5));
		assertTrue(!pk5.equals(pk7));
		assertTrue(!pk7.equals(pk8));
		assertFalse(pk9.equals(pk10));
		System.out.println("hashCode 9 "+pk9.hashCode());
		System.out.println("hashCode 10 "+pk10.hashCode()); 
	}
}
