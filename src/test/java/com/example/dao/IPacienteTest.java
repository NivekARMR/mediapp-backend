package com.example.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Paciente;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IPacienteTest {


	@Autowired
	private IPacienteDAO dao;
 

	@Test
	public void crearPaciente() {
		Paciente us = new Paciente();
		us.setIdPaciente(1);
		us.setNombres("kevin");
		us.setApellidos("meneses");
		us.setDireccion("Vrela 1176"); 
		us.setDni("71819234");
		us.setTelefono("957264681");

		dao.save(us);
		assertTrue(dao.count()>0);

	} 

	@Test
	public void buscarPaciente() {

		Paciente us = new Paciente();
		us.setIdPaciente(5);
		us.setNombres("kevin arthur");
		us.setApellidos("meneses rivera");
		us.setDireccion("Varela 1176"); 
		us.setDni("71856345");
		us.setTelefono("957264681");
		

		Paciente nuevo=dao.save(us);

		assertTrue(nuevo.getApellidos()!=null);
		assertTrue(nuevo.getDireccion()!=null);
		assertTrue(nuevo.getDni()!=null);
		assertTrue(nuevo.getNombres()!=null);
		assertTrue(nuevo.getTelefono()!=null);
		assertTrue(dao.count()>0);

	}
	 
	@Test
	public void ListPaciente() {

		Paciente us = new Paciente();
		us.setIdPaciente(11);
		us.setNombres("kevin");
		us.setApellidos("meneses");
		us.setDireccion("Vrela 1176"); 
		us.setDni("71819234");
		us.setTelefono("957264681");
		dao.save(us);

		
		System.out.println("dao.count 1"+dao.count());
		Paciente us2= new Paciente();
		us2.setIdPaciente(12);
		us2.setNombres("kevin");
		us2.setApellidos("meneses");
		us2.setDireccion("Vrela 1176"); 
		us2.setDni("71819234");
		us2.setTelefono("957264681");
		dao.save(us2);
		System.out.println("dao.count 2"+dao.count());

		dao.delete(us2);

		System.out.println("dao.count 3"+dao.count());

		assertTrue(dao.count()>0);

	}
	
}
