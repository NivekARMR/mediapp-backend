package com.example.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.dao.IUsuarioDAO;
import com.example.dao.IResetTokenDAO;
import com.example.model.ResetToken;
import com.example.model.Rol;
import com.example.model.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IUsuarioTest {

	@Autowired
	private IUsuarioDAO usuariodao;

	@Autowired
	private IResetTokenDAO ResetTokenDAO;
	
	@Autowired
	private IRolDAO roldao;

	
	@Autowired
	private BCryptPasswordEncoder bcrypt;

	@Test
	public void crearUsuario() {

		Rol rol = new Rol();
		rol.setDescripcion("Admin");
		rol.setIdRol(1);
		rol.setNombre("Administrator");
		Rol nuevo=roldao.save(rol);
		
		List<Rol> roles= new ArrayList();
		roles.add(nuevo);
		
		Usuario us = new Usuario();
		us.setIdUsuario(4);
		us.setUsername("mito");
		us.setPassword(bcrypt.encode("321"));		
		us.setEnabled(true);
		us.setRoles(roles);
		
		Usuario nuevou = usuariodao.save(us);
		assertTrue(nuevou.getPassword().equalsIgnoreCase((us.getPassword())));
		assertTrue(nuevou.getIdUsuario()==((us.getIdUsuario())));
		assertTrue(nuevou.getUsername()==((us.getUsername())));
		assertTrue(nuevou.isEnabled()==us.isEnabled());
		assertTrue(nuevou.getPassword().equalsIgnoreCase((us.getPassword())));
		assertTrue(nuevo.getDescripcion().equals(rol.getDescripcion()));
		assertTrue(nuevo.getIdRol()==(rol.getIdRol()));
		assertTrue(nuevo.getNombre()==(rol.getNombre()));
		assertTrue(nuevou.getRoles()==(nuevou.getRoles()));
		
		ResetToken resettoken= new ResetToken();
			resettoken.setId((long)1);
			resettoken.setToken("AizaSWERTYUIKA");
			resettoken.setUsuario(nuevou);
			resettoken.setExpiracion(5);
			resettoken.setExpiracion(new Date());
			ResetToken nuevotoken= ResetTokenDAO.save(resettoken);

			assertTrue(resettoken.getId()==(nuevotoken.getId()));
 			assertTrue(resettoken.getToken()==(nuevotoken.getToken()));
 			assertTrue(resettoken.isExpirado()==true);
 			assertTrue(resettoken.getUsuario()!=null);
 			assertTrue(resettoken.getExpiracion()!=null);
			
			
	}
}
