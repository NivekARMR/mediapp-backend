package com.example.dao;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
 
import com.example.model.Examen;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IExamenTest {


	@Autowired
	private IExamenDAO examendao;
  
	@Test
	public void crearExamen() {
		
		Examen examen= new Examen();
		examen.setIdExamen(1);
		examen.setNombre("Examen de Orina");
		examen.setDescripcion("Examen de Orina"); 
		Examen nuevo=examendao.save(examen);
 		  
		assertTrue(nuevo.getDescripcion()!=null);
		assertTrue(nuevo.getIdExamen()==1);
		assertTrue(nuevo.getNombre()!=null); 
		} 
 	
}
