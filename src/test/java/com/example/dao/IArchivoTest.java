package com.example.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Archivo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IArchivoTest {


	@Autowired
	private IArchivoDAO archivodao;
  
	@Test
	public void crearArchivo() {
		byte[] data="imagen".getBytes();
		Archivo archivo= new Archivo();
		archivo.setFilename("nueva foto");
		archivo.setFiletype(".jpg");
		archivo.setValue(data);
		archivo.setIdArchivo(1); 
		Archivo nuevo=archivodao.save(archivo);
		
		assertTrue(nuevo.getFilename().equals(archivo.getFilename()));
		assertTrue(nuevo.getFiletype().equals(archivo.getFiletype())); 
		assertTrue(nuevo.getIdArchivo()==(archivo.getIdArchivo())); 
		assertTrue(nuevo.getValue()!=null); 
	}
}
