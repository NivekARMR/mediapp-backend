package com.example.util;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp ; 
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IMailTest {

 	EmailService email;
	@Before
	public void setUp() throws Exception {
 
	}
	@Test
	public void test() {
		Mail mail= new Mail();
		mail.setFrom("kevin60_2@gmail.com");
		mail.setTo("kevin60_2@gmail.com");
		mail.setSubject("Model");
		Map<String,Object> a= new HashMap();
		a.put("1","Cuerpo");
		mail.setModel(a);


		long millis=System.currentTimeMillis();
		java.sql.Date date2=new java.sql.Date(millis);
		
		assertTrue(mail.getFrom()!=null);
		assertTrue(mail.getTo()!=null);
		assertTrue(mail.getSubject()!=null);
		assertTrue(mail.getFrom()!=null);
		assertTrue(mail.getModel()!=null);
	
		MyLocalDateConverter mldc= new MyLocalDateConverter();
		 assertTrue(mldc.convertToDatabaseColumn(LocalDate.now())!=null);
		 assertTrue(mldc.convertToEntityAttribute(date2)!=null); 	

		 assertTrue(mldc.convertToDatabaseColumn(null)==null);
		 assertTrue(mldc.convertToEntityAttribute(null)==null); 	

		 MyLocalDateTimeConverter dtc= new MyLocalDateTimeConverter();
		 assertTrue(dtc.convertToDatabaseColumn(null)==null);
		 assertTrue(dtc.convertToEntityAttribute(null)==null);
 		 assertTrue(dtc.convertToDatabaseColumn(LocalDateTime.now())!=null);
		 assertTrue(dtc.convertToEntityAttribute(new Timestamp(System.currentTimeMillis()))!=null);
	 
	 }

}
