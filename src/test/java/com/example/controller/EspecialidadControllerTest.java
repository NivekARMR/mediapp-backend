package com.example.controller;

import static org.junit.Assert.*; 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.AbstractTest;
import com.example.model.Especialidad;

import org.junit.Test;

public class EspecialidadControllerTest extends AbstractTest  {

	  @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	  
	  @Test
	   public void getEspecialidades() throws Exception {
	      String uri = "/especialidades";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      Especialidad[] especialidadlist = super.mapFromJson(content, Especialidad[].class);
	      assertTrue(especialidadlist.length>0);
	   }
	  
	  @Test
	   public void registrarEspecialidad() throws Exception {
	      String uri = "/especialidades";
	      Especialidad especialidad = new Especialidad();
	      especialidad.setIdEspecialidad(1);
	      especialidad.setNombre("Cirugìa");
	      String inputJson = super.mapToJson(especialidad);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      System.out.println("registrar status"+status); 
	      assertEquals(201, status);
	      String content = mvcResult.getResponse().getContentAsString();
	   }
	  

		  @Test
		   public void actualizaEspecialidad() throws Exception {
		      String uri = "/especialidades";
		      Especialidad especialidad = new Especialidad();
		      especialidad.setIdEspecialidad(1);
		      especialidad.setNombre("Cirugia");
		      String inputJson = super.mapToJson(especialidad);
		      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
		         .contentType(MediaType.APPLICATION_JSON_VALUE)
		         .content(inputJson)).andReturn();
		      
		      int status = mvcResult.getResponse().getStatus();
		      System.out.println("status"+status); 
		      assertEquals(200, status);
		      String content = mvcResult.getResponse().getContentAsString();
		   }
		  
	   
	   }
