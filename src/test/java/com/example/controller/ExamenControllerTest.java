package com.example.controller;

import static org.junit.Assert.*; 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.AbstractTest;
import com.example.model.Examen;

import org.junit.Test;

public class ExamenControllerTest extends AbstractTest  {

	  @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	  
	  @Test
	   public void getexamenes() throws Exception {
	      String uri = "/examenes";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      Examen[] exameneseslist = super.mapFromJson(content, Examen[].class);
	      assertTrue(exameneseslist.length>=0);
	   }
	  
	  @Test
	   public void registrarExamen() throws Exception {
	      String uri = "/examenes";
	      Examen examen = new Examen();
	      examen.setIdExamen(1);
	      examen.setNombre("Cirugìa");
	      examen.setDescripcion("Cirugia General"); 
	      String inputJson = super.mapToJson(examen);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      System.out.println("registrar status"+status); 
	      assertEquals(201, status);
	      String content = mvcResult.getResponse().getContentAsString();
	   }
	  

		  @Test
		   public void actualizaExamen() throws Exception {
		      String uri = "/examenes";
		      Examen examen = new Examen();
		      examen.setIdExamen(1);
		      examen.setNombre("Cirugia");
		      examen.setDescripcion("Cirugia");
		      String inputJson = super.mapToJson(examen);
		      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
		         .contentType(MediaType.APPLICATION_JSON_VALUE)
		         .content(inputJson)).andReturn();
		      
		      int status = mvcResult.getResponse().getStatus();
		      System.out.println("status"+status); 
		      assertEquals(200, status);
		      String content = mvcResult.getResponse().getContentAsString();
		   }
		  
	   
	   }
