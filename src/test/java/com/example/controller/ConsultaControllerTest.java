package com.example.controller;

import static org.junit.Assert.*; 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.AbstractTest;
import com.example.model.Consulta;

public class ConsultaControllerTest extends AbstractTest  {

	  @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	  
	  @Test
	   public void getConsultas() throws Exception {
	      String uri = "/consultas";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      Consulta[] especialidadlist = super.mapFromJson(content, Consulta[].class);
	      assertTrue(especialidadlist.length>=0);
	   }
	}
