package com.example.controller;

import static org.junit.Assert.*; 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.AbstractTest;
import com.example.model.Especialidad;
import com.example.model.Medico;
import com.example.model.Menu;

import org.junit.Test;

public class MenuControllerTest extends AbstractTest  {

	  @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	  
	  @Test
	   public void getMenus() throws Exception {
	      String uri = "/menus";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      Menu[] menuslist = super.mapFromJson(content, Menu[].class);
	      assertTrue(menuslist.length>=0);
	   }
 }
