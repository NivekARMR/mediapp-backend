package com.example;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.boot.builder.SpringApplicationBuilder;

public class MediappBackendApplicationTest {
  
	@Test
	public void mainTest() {
		String[] cars = {"", ""};
		MediappBackendApplication.main(cars); 

		assertTrue(cars!=null);
	}
	 
	@Test
	public void builderTest() {
		SpringApplicationBuilder springApplicationBuilder= new SpringApplicationBuilder();
		MediappBackendApplication mediappBackendApplication= new MediappBackendApplication();
		mediappBackendApplication.configure(springApplicationBuilder);
		assertTrue(mediappBackendApplication!=null);
	}

}
